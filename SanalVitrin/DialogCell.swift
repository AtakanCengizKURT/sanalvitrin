//
//  DialogCell.swift
//  SanalVitrin
//
//  Created by Atakan Cengiz KURT on 21.12.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class DialogCell: UICollectionViewCell {
    
    var delegate: DialogCollectionViewCellDelegate?
    var index = 0
    
    @IBOutlet weak var screenButton: UIButton!
    @IBOutlet weak var screenLabel: UILabel!
    
    @IBAction func screenImageButton(_ sender: Any) {
        delegate?.screenImageButton(index: index)
    }
    
}


protocol DialogCollectionViewCellDelegate {
    func screenImageButton(index: Int)
}
