//
//  Dialog.swift
//  SanalVitrin
//
//  Created by Atakan Cengiz KURT on 21.12.2018.
//  Copyright © 2018 Atakan Cengiz KURT. All rights reserved.
//

import UIKit

class Dialog: UIViewController {

    var delegate : DialogDelegate?
    
    let images = ["Screen1","Screen2","Screen3"]
    let titles = ["iPhone1","iPhone2","iPhone3"]
    let screens = ["iPhone-x1","iPhone-x2","iPhone-x3"]
    
    @IBOutlet weak var screenCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        screenCollectionView.delegate = self
        screenCollectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    



}

extension Dialog : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "screen", for: indexPath) as! DialogCell
        
        cell.screenButton.setImage(UIImage(named: screens[indexPath.row]), for: .normal)
        cell.screenLabel.text = titles[indexPath.row]
        cell.index = indexPath.row
        cell.delegate = self
        
        return cell
    }
    
    
    
    
    
}

extension Dialog: DialogCollectionViewCellDelegate{
    func screenImageButton(index: Int) {
        dismiss(animated: true, completion: nil)
        delegate!.screenImageButton(image: UIImage(named: images[index])!)
    }
    
}

protocol DialogDelegate {
    func screenImageButton(image: UIImage)
}
